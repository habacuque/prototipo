import React from 'react'
import { TextInput } from 'react-native'

import styles from './styles'

interface SearchBarProps { placeholder: string , onChangeText: Function }
export default function SearchBar(props: SearchBarProps) {
    
    return (
        <TextInput style={styles.input} placeholder={props.placeholder} onChangeText={text => props.onChangeText(text.trim())}/>
    )
}

