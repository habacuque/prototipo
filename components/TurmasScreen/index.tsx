import React from 'react'
import { FlatList } from 'react-native'
import { useRoute } from '@react-navigation/native'

import FullScreen from 'prototipo/components/shared/FullScreen'
import Header from 'prototipo/components/shared/Header'
import SubHeader from 'prototipo/components/shared/SubHeader'
import SearchBar from 'prototipo/components/shared/SearchBar'
import SimpleList from 'prototipo/components/shared/SimpleList'

import UnidadeModel from 'prototipo/components/UnidadesScreen/UnidadeModel'
import TurmaModel from './TurmaModel'
import styles from './styles'
import Service from './Service'

interface RouteParams { unidade: UnidadeModel }
export default function TurmasScreen(props: TurmasScreenProps) {

  const route = useRoute<RouteParams>()
  const unidade = route.params.unidade
  const [searchValue,setSearchValue] = React.useState('')
  const [turmas,setTurmas] = React.useState<TurmaModel[]>([])

  React.useEffect( () => {
     const turms = Service.getTurmas(unidade.key)
     setTurmas(turms) 
  },[])


  function onSearch(text: string) {
    setSearchValue(text)
  }

  return (
    <FullScreen>
        <Header value={unidade.nome}/>
        <SubHeader value={"turmas da unidade no bairro "+unidade.bairro+" em "+unidade.cidade}/>
        <SearchBar placeholder='clique aqui para pesquisar turmas' onChangeText={onSearch} />
        <SimpleList 
            data={turmas} 
            onItemClick={ () => console.log('click') } 
            itemToInfo={ (item:TurmaModel) => { 
                const diasComAula:string = Object.keys(item.horarios).filter( (dia) => item.horarios[dia] ).toString().replace(/,/g,'  ')
                return [`professor(a): ${item.professor}`,diasComAula] 
            }}
        />
    </FullScreen>
  )

}
