const styles = {

    header: {
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: 'stretch',
        alignItems: 'center',
    },

    headerText: {
        fontSize: 35,
        fontFamily: 'serif',
        color: '#00bfff',
    },
    
}

export default styles
