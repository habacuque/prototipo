import React from 'react'
import { FlatList } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import FullScreen from 'prototipo/components/shared/FullScreen'
import Header from 'prototipo/components/shared/Header'
import SearchBar from 'prototipo/components/shared/SearchBar'
import SimpleList from 'prototipo/components/shared/SimpleList'

import UnidadeModel from './UnidadeModel'
import styles from './styles'
import Service from './Service'

export default function UnidadesScreen() {

  const [searchValue,setSearchValue] = React.useState('')
  const [unidades,setUnidades] = React.useState<UnidadeModel[]>([])
  const navigation = useNavigation()

  React.useEffect( () => {
     const unids = Service.getInstituicoes()
     setUnidades(unids) 
  },[])


  function onSearch(text: string) {
    setSearchValue(text)
  }

  return (
    <FullScreen>
        <Header value='Unidades'/>
        <SearchBar placeholder='clique aqui para pesquisar' onChangeText={onSearch} />
        <SimpleList 
            data={unidades}
            onItemClick={ (item:UnidadeModel) => navigation.navigate('UnidadeScreen', {unidade: item})}
            itemToInfo={ (item:UnidadeModel) => [
               `cidade ${item.cidade}`,
               `bairro ${item.bairro}`,
            ]}
            
        />
    </FullScreen>
  )

}
