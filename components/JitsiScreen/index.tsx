import React from 'react'
import { Text } from 'react-native'

import JitsiMeet, { JitsiMeetView } from 'react-native-jitsi-meet'

const url = 'https://meet.jit.si/exemple'
const userInfo = { displayName: 'User', email: 'user@example.com', avatar: 'https:/gravatar.com/avatar/abc123' }

export default function JitsiScreen() {

    React.useEffect(() => {
        const delay = setTimeout(() => {
          JitsiMeet.call(url,userInfo)
          return () => { JitsiMeet.endCall() ; clearTimeout(delay) }
        }, 1000);
    }, [])

  return (
    <JitsiMeetView
      onConferenceTerminated={e => console.log('terminado')}
      onConferenceJoined={e => console.log('entrou')}
      onConferenceWillJoin={e => console.log('vai entrar')}
      style={{ flex: 1, height: '100%', width: '100%', }}
    />
  )

}
