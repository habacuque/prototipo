import React from 'react'
import { Text } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import Service from './Service'
import FullScreen from 'prototipo/components/shared/FullScreen'
import Header from 'prototipo/components/shared/Header'
import SubHeader from 'prototipo/components/shared/SubHeader'
import SimpleMenu from 'prototipo/components/shared/SimpleMenu'

export default function HomeScreen() {

    const navigation = useNavigation()
    
    const [lembretes,setLembretes] = React.useState([])    
    const [matriculas,setMatriculas] = React.useState([])

    React.useEffect( () => {
        const lemb = Service.getLembretes()
        const matr = Service.getMatriculas()
        setLembretes(lemb)
        setMatriculas(matr)
    },[]) 

    return (
        <FullScreen>
            <Header value='Bem vinda(o)'/>
            <SubHeader value='Parece que você ainda não está matriculado(a) em nenhuma instituição'/>
            <SimpleMenu actions={[
                {name:'Clique aqui para procurar instituições',func: () => navigation.navigate('UnidadesScreen') }
            ]}/>
        </FullScreen>
        )
    }
