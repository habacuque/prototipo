import React from 'react'
import { View } from 'react-native'

import styles from './styles'

interface FullScreenProps { children: React.ReactNode }
export default function FullScreen(props: FullScreenProps) {
    return (
        <View style={styles.container}>
            {props.children}
        </View>
    )
}
