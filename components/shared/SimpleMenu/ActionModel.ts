export default interface Action {
    name: string,
    func: Function
}
