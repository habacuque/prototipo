const styles = {

    touch: {
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'stretch',
    },
    
    image: {
        height: 100,
        width: 100,
    },

    infoContainer: {
        paddingLeft: 5,
        flex: 1,
    },

    nomeText: {
        color: '#00bfff',
        fontSize: 20,
    },

}

export default styles
