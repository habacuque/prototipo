import React from 'react'
import { View , Button } from 'react-native'
import { useRoute , useNavigation } from '@react-navigation/native'

import FullScreen from 'prototipo/components/shared/FullScreen'
import UnidadeModel from 'prototipo/components/UnidadesScreen/UnidadeModel'
import Header from 'prototipo/components/shared/Header'
import SubHeader from 'prototipo/components/shared/SubHeader'
import SimpleMenu from 'prototipo/components/shared/SimpleMenu'

import styles from './styles'

interface RouteParams { unidade: UnidadeModel } 
export default function UnidadesScreen() {

  const route = useRoute<RouteParams>()
  const navigation = useNavigation()
  const unidade = route.params.unidade

  return (
    <FullScreen>
        <Header value={unidade.nome}/>
        <SubHeader value="unidade: endereco"/>
        <SubHeader value={"contato: "+unidade.contato}/>
        <SimpleMenu actions={[
            {name:'Matricular-se em aulas desta unidade',func: () => navigation.navigate('TurmasScreen',{unidade:unidade}) },
            {name:'Visualizar avisos desta unidade',func: () => console.log('click') },
        ]}/>
    </FullScreen>
  )

}

