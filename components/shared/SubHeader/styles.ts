const styles = {

    header: {
        marginLeft: 20,
        marginRight: 20,
        paddingLeft: 20,
        alignSelf: 'stretch',
        alignItems: 'center',
    },

    headerText: {
        fontSize: 15,
        color: '#1b9cd0',
    },
    
}

export default styles
