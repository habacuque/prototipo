const styles = {

    input: {
        padding: 5,
        borderRadius: 20,
        textAlign: 'center',
        backgroundColor: 'white',
        elevation: 1,
        width: '75%',
        marginTop: 20,

    },

}

export default styles
