import SimpleListableModel from 'prototipo/components/shared/SimpleList/SimpleListableModel'

interface Horarios {
    seg?: string,
    ter?: string,
    quar?: string,
    quin?: string,
    sex?: string,
    sab?: string,
    dom?: string,
}


export default interface TurmaModel extends SimpleListableModel {
    key: string,
    imageUri: string,
    nome: string,
    professor: string,
    horarios: Horarios,
}
