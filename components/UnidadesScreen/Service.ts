import UnidadeModel from './UnidadeModel'

class Service {

    getInstituicoes() : UnidadeModel[] {
        return [ 
            { nome: 'nome instituicao' , imageUri: 'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg', cidade: 'nome' , bairro: 'nome', rua: 'nome', numero: 123 , complemento: 'terceiro andar', contato: '123456789', key:'0' }, 
            { nome: 'nome instituicao' , imageUri: 'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg', cidade: 'nome' , bairro: 'nome', rua: 'nome', numero: 123 , complemento: 'terceiro andar', contato: '123456789', key:'1' }, 
            { nome: 'nome instituicao' , imageUri: 'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg', cidade: 'nome' , bairro: 'nome', rua: 'nome', numero: 123 , complemento: 'terceiro andar', contato: '123456789', key:'2' }, 
            { nome: 'nome instituicao' , imageUri: 'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg', cidade: 'nome' , bairro: 'nome', rua: 'nome', numero: 123 , complemento: 'terceiro andar', contato: '123456789', key:'3' }, 
            { nome: 'nome instituicao' , imageUri: 'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg', cidade: 'nome' , bairro: 'nome', rua: 'nome', numero: 123 , complemento: 'terceiro andar', contato: '123456789', key:'4' }, 
        ]
    }

}

export default new Service()
