import React from 'react'
import { View , Text } from 'react-native'

import styles from './styles'

interface SubHeaderProps { value: string }
export default function SubHeader(props: SubHeaderProps) {

  return (
    <View style={styles.header}>
        <Text style={styles.headerText}>{props.value}</Text>
    </View>
  )

}

