import React from 'react'
import { View , Text , Image, TouchableHighlight } from 'react-native'

import styles from './styles'
import SimpleListableModel from '../../SimpleListableModel'

interface SimpleCardProps { item: SimpleListableModel , itemInfo: string[] , onClick: Function }
export default function SimpleCard(props: SimpleCardProps) {
    return (
        <TouchableHighlight style={styles.touch} underlayColor='white' activeOpacity={0.5} onPress={ () => props.onClick(props.item) }>
        <View style={styles.container}>
            <Image style={styles.image} source={{uri:props.item.imageUri}}/>
            <View style={styles.infoContainer}>
                <Text style={styles.nomeText}>{props.item.nome}</Text>
                { props.itemInfo.map ( (info,index) => <Text key={index}>{info}</Text>)}
            </View>
        </View>
        </TouchableHighlight>
    )
}
