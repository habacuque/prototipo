import React from 'react'
import { View , Text } from 'react-native'

import styles from './styles'

interface HeaderProps { value: string }
export default function Header(props: HeaderProps) {

  return (
    <View style={styles.header}>
        <Text style={styles.headerText}>{props.value}</Text>
    </View>
  )

}

