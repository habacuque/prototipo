import 'react-native-gesture-handler' // deve estar obrigatoriamente no topo, https://reactnavigation.org/docs/getting-started/

import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import HomeScreen from 'prototipo/components/HomeScreen'
import UnidadesScreen from 'prototipo/components/UnidadesScreen'
import UnidadeScreen from 'prototipo/components/UnidadeScreen'
import TurmasScreen from 'prototipo/components/TurmasScreen'
import JitsiScreen from 'prototipo/components/JitsiScreen'

const Stack = createStackNavigator()

export default function App() {
  return ( 
    <NavigationContainer> 
      <Stack.Navigator initialRouteName={"HomeScreen"}>
        <Stack.Screen options={{headerShown:false}} name="HomeScreen" component={HomeScreen} />
        <Stack.Screen options={{headerShown:false}} name="UnidadesScreen" component={UnidadesScreen} />
        <Stack.Screen options={{headerShown:false}} name="UnidadeScreen" component={UnidadeScreen} />
        <Stack.Screen options={{headerShown:false}} name="TurmasScreen" component={TurmasScreen} />
        <Stack.Screen options={{headerShown:false}} name="JitsiScreen" component={JitsiScreen} />
      </Stack.Navigator>
    </NavigationContainer> 
  )
}
