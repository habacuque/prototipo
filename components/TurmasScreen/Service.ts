import TurmaModel from './TurmaModel'

class Service {

    getTurmas(unidadeKey: string) : TurmaModel[] {
        return [ 
            { 
                key: '0' , 
                imageUri:  
                'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg' , 
                nome: 'nome' , 
                professor: 'nome' , 
                horarios: { 
                    seg: '14:00 - 15:00',
                    ter: '14:00 - 15:00',
                    quar: '14:00 - 15:00',
                    quin: '14:00 - 15:00',
                    sex: '14:00 - 15:00',
                    sab: '14:00 - 15:00',
                    dom: '14:00 - 15:00',
                }
            }, 
            { 
                key: '1' , 
                imageUri:  
                'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg' , 
                nome: 'nome' , 
                professor: 'nome' , 
                horarios: { 
                    seg: '14:00 - 15:00',
                    quar: '14:00 - 15:00',
                }
            }, 
            { 
                key: '2' , 
                imageUri:  
                'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg' , 
                nome: 'nome' , 
                professor: 'nome' , 
                horarios: { 
                    seg: '14:00 - 15:00',
                    quar: '14:00 - 15:00',
                    quin: '14:00 - 15:00',
                    sex: '14:00 - 15:00',
                }
            }, 
            { 
                key: '3' , 
                imageUri:  
                'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg' , 
                nome: 'nome' , 
                professor: 'nome' , 
                horarios: { 
                    seg: '14:00 - 15:00',
                    quar: '14:00 - 15:00',
                    quin: '14:00 - 15:00',
                    sab: '14:00 - 15:00',
                }
            }, 
            { 
                key: '4' , 
                imageUri:  
                'https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg' , 
                nome: 'nome' , 
                professor: 'nome' , 
                horarios: { 
                    seg: '14:00 - 15:00',
                    ter: '14:00 - 15:00',
                    quar: '14:00 - 15:00',
                    dom: '14:00 - 15:00',
                }
            }, 
        ]
    }

}

export default new Service()
