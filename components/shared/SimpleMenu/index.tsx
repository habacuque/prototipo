import React from 'react'
import { View , Button , Text } from 'react-native'

import styles from './styles'
import ActionModel from './ActionModel'

interface SimpleMenuParams { actions: ActionModel[] }
export default function SimpleMenu(props: SimpleMenuParams) {

    const menuItems = props.actions.map( (action,index) => {
        return (
            <View key={index} style={styles.item}>
                <Button title={action.name} onPress={action.func} />
            </View>
        )
    })

    return (
        <View style={styles.container}>
            {menuItems}
        </View>
    )
}
