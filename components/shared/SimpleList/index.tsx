import React from 'react'
import { PropsWithChildren } from 'react'
import { FlatList } from 'react-native'

import SimpleCard from './components/SimpleCard'
import SimpleListableModel from './SimpleListableModel'
import styles from './styles'

//an generic list 


interface ItemToInfoFunction { (item: SimpleListableModel) : string[] } // converts an item to the data we want to show
interface SimpleListProps<T> { data: T[] , onItemClick: Function , itemToInfo: ItemToInfoFunction }
export default function SimpleList<T extends SimpleListableModel>(props: PropsWithChildren< SimpleListProps<T> >){
    return <FlatList style={styles.list} 
        data={props.data} 
        renderItem={ ({item}) => <SimpleCard onClick={props.onItemClick} itemInfo={props.itemToInfo(item)} item={item} /> }
    />

}


