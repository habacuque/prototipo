import SimpleListableModel from 'prototipo/components/shared/SimpleList/SimpleListableModel'
export default interface UnidadeModel extends SimpleListableModel {
    key: string,
    imageUri: string,
    nome: string,
    cidade: string,
    bairro: string,
    rua: string,
    numero: number,
    complemento: string,
    contato: string,
}
